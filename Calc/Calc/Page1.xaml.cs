﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Calc
{
    public partial class Page1 : ContentPage
    {
        string st;
        bool flag;
        bool dotFlag;
        double result;

        public Page1()
        {
            InitializeComponent();
            flag = false;
            dotFlag = false;
            result = 0;
        }


        
        void OnClickedCE(object sender, EventArgs args) {
            labelText.Text = "0";
            result = 0;
            dotFlag = false;
        }

        void OnClickedChangeSign(object sender, EventArgs args)
        {
            double num = -1*Convert.ToDouble(labelText.Text);
            labelText.Text = num.ToString();
        }


        void OnClickedProcent(object sender, EventArgs args)
        {
            Button btn = (Button)sender;
           
        }




        void OnClickedSign(object sender, EventArgs args)
        {
            Button btn = (Button)sender;
            st = btn.Text;
            result = Convert.ToDouble(labelText.Text);
            flag = true;
            dotFlag = false;
        }


        void OnClickedNumber(object sender, EventArgs args)
        {
            Button btn = (Button)sender;
            if (flag)
            {
                labelText.Text = "0";

            }
            flag = false;
            labelText.Text += btn.Text.ToString();
        }



        void OnClickedDot(object sender, EventArgs args)
        {
            Button btn = (Button)sender;
            
            if(!dotFlag)
            labelText.Text += Device.OnPlatform(",",",",".");
            dotFlag = true;
        }


        



        void OnClickedEqual(object sender, EventArgs args)
        {
            if (st == "+")
            {
                labelText.Text = (result + Convert.ToDouble(labelText.Text)).ToString();

            }

            if (st == "-")
            {
                labelText.Text = (result - Convert.ToDouble(labelText.Text)).ToString();

            }

            if (st == "*")
            {
                labelText.Text = (result * Convert.ToDouble(labelText.Text)).ToString();

            }

            if (st == "/")
            {
                if (Convert.ToDouble(labelText.Text) != 0)
                    labelText.Text = (result / Convert.ToDouble(labelText.Text)).ToString();
                else
                {
                    labelText.Text = "Devision by 0";

                }
            }
        }





    }
}
